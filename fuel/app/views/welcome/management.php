<!doctype html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofllow">

  <!--jQueryの読み込み-->
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <!--読み込まれたら次の処理をする-->
  <script>
  $(function() {
    //".edit_area, .delete_area"を隠しておく
    $(".edit_area, .delete_area").hide();

    //クラスである".edit"のボタンをクリックしたとき
    $(".edit").click(function() {
      //".delete_area"を隠しておく
      $(".delete_area").hide();
      // this=>".edit"　, slideToggle()でON/OFF切り替えたスライドをする
      $(this).parents(".article").children(".edit_area").slideToggle();
    });

    //クラスである".delete"のボタンをクリックしたとき
    $(".delete").click(function() {
      //".edit_area"を隠しておく
      $(".edit_area").hide();
      // this=>".delete"　, slideToggle()でON/OFF切り替えたスライドをする
      $(this).parents(".article").children(".delete_area").slideToggle();
    });

    //submit-eクラスのボタンが押されたときの処理
    $(document).on('click', '.submit-e', function check(){
      var user_name = $(this).parents("form[name='edit_form']").children("div.form-group").children('.user-name');
      var write_area = $(this).parents("form[name='edit_form']").children("div.form-group").children('.write-area');
      var str_mn=user_name.val();
      var str_mw=write_area.val();
      var rep_mn=str_mn.replace(/[\s　\t\n\r]/g,"");
      var rep_mw=str_mw.replace(/[\s　\t\n\r]/g,"");
      //val()関数はvalueの値を取得する
      //入力チェック
      // 名前と本文がどちらも未入力
      if ((str_mn === "" && str_mw === "")||(rep_mn === "" && rep_mw === "")) {
        // 赤字でメッセージ
        $("#none_input_name_m").show();
        $("#none_input_text_m").show();
        return false;
      }
      // なまえが未入力
      else if (str_mn === "" || rep_mn === "") {
        $("#none_input_name_m").show();
        $("#none_input_text_m").hide();
        return false;
      }
      // 本文が未入力
      else if (str_mn === "" || rep_mw === "") {
        $("#none_input_name_m").hide();
        $("#none_input_text_m").show();
        return false;
      }
    });
  });
  </script>

  <link rel="icon" href="../assets/img/favicon.ico">
  <title>弘太郎ちゃん掲示板 | 管理画面</title>
  <link rel="stylesheet" href="../assets/css/bootstrap.css">
  <link rel="stylesheet" href="../assets/css/custom.css">
</head>

<body class="management">

  <?php
  //掲示板の番号
  $count = 1;
  ?>
  <div class="header">
    <div class="contents">
      <h1 class="text_center title">掲示板管理ページ</h1>
      <div class="btn_area">
        <a href="http://192.168.33.41/" class="btn btn-block btn-default">掲示板にもどる</a>
      </div>
    </div>
  </div>

  <div class="list">
    <div class="contents">
      <?php
      //配列の最後まで繰り返す
      foreach ($cursor as $value) {
        ?>
        <div class="article">
          <div class="article_over">
            <span><?php echo $count; ?></span>
            <span class="name">名前 : <?php echo $value['user_name'] ?></span>
            <span class="date"><?php echo date('Y年m月d日', $value['timestamp']);?></span>
            <span class="time" value=<?php $value['timestamp']?>>
              <?php
              if(empty($value['time2'])){
                echo nl2br("\n");
                echo ($value['time']);
              }
              else{
                echo nl2br("\n");
                echo($value['time']."　");
                echo($value['time2']);
              }
              ?>
            </span>
          </div>

          <div class="article_under">
            <p><?php echo nl2br($value['write_area']); ?></p>
          </div>

          <div class="article_btns">
            <div class="edit">
              <button type="button" class="btn btn-block btn-default">編集するん？</button>
            </div>
            <div class="delete">
              <button type="button" class="btn btn-block btn-warning">消しちゃう？</button>
            </div>
          </div>

          <div class="edit_area">
            <!--methodでpostformであることを宣言　, actionでどこにpostの値を投げるかを指定。action=””だと自分のソースファイルの飛ぶ-->
            <!-- 今回Controller_Managementのaction_updateへ -->
            <form name="edit_form" role="form" action="update" method="post" onsubmit="return check">
              <div class="form-group">
                <input name="user_name" type="text" class="form-control user-name" maxlength="8" value=<?php echo $value['user_name'];?>>
                <p id="none_input_name_m" style="display:none; color:red;">なまえを入力してください</p><!--もしなまえが入っていなかったら表示する-->
              </div>
              <div class="form-group">
                <textarea name="write_area" class="form-control write-area" rows="5" cols="40" ><?php echo $value['write_area']; ?></textarea>
                <p id="none_input_text_m" style="display:none; color:red;">本文を入力してください</p><!--もし本文が入っていなかったら表示する-->
              </div>
              <!--nameでポストを飛ばす-->
              <input type="hidden" name="mongo_id_e" value=<?php echo $value['_id']; ?>>
              <input type="submit" name="submit_e" class="btn btn-block btn-primary submit-e" value="編集するん？">
            </form>
          </div>

          <!--methodでpostformであることを宣言　, actionでどこにpostの値を投げるかを指定。action=””だと自分のソースファイルの飛ぶ-->
          <form class="delete_area" action="delete" method="post">
            <p class="text_center">消しちゃいますよー？いいっすか？</p>
            <!--nameでポストを飛ばす-->
            <input type="hidden" name="mongo_id_d" value=<?php echo $value['_id']; ?>>
            <input type="submit" name="submit_d" class="btn btn-block btn-danger" value="消しちゃいましょう！">
          </form>
        </div>
        <?php
        $count++;
      }
      ?>
    </div>
  </div>
</body>
<script src="../assets/javascripts/bootstrap.js"></script>
</html>
