<!doctype html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex,nofllow">

  <!-- jqueryの読み込み -->
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>

  <script>
  //入力チェックの関数
  function check() {
    var str_n=document.write_form.user_name.value;
    var str_w=document.write_form.write_area.value;
    // 改行、スペース等のみで空白になっている場合を判定するのに使う変数
    // 改行、スペースを""に置き換えている
    var rep_n=str_n.replace(/[\s　\t\n\r]/g,"");
    var rep_w=str_w.replace(/[\s　\t\n\r]/g,"");

    // nameを指定し、valueの値が空でないかを確認
    // 名前と本文が空欄、または改行、スペース等のみで空白になっている場合
    if ((str_n === "" && str_w === "")||(rep_n === "" && rep_w === "")) {
      $("#none_input_name").show();
      $("#none_input_text").show();
      return false;// 送信中止
    }
    // 名前が空欄、または改行、スペース等のみで空白になっている場合
    else if (str_n === "" || rep_n === "") {
      $("#none_input_name").show();
      $("#none_input_text").hide();
      return false;
    }
    // 本文が空欄、または改行、スペース等のみで空白になっている場合
    else if (str_w === "" || rep_w === "") {
      $("#none_input_name").hide();
      $("#none_input_text").show();
      return false;
    }
  }
  </script>

  <link rel="icon" href="..//assets/img/favicon.ico">
  <title>弘太郎ちゃん掲示板</title>
  <link rel="stylesheet" href="../assets/css/bootstrap.css">
  <link rel="stylesheet" href="../assets/css/custom.css">
</head>

<body class="bbs">
  <div class="top_area">
    <div class="header">
      <div class="contents">
        <h1 class="text_center title">KOTARO's Bulletin Board System</h1>

        <!-- 下記のAにてmanagement.phpにとばす -->
        <p class="text_right">Designed by MasaNAKAMUR<a href="management/index">A</a></p>
      </div>
    </div>
    <div class="input">
      <div class="contents">
        <div class="input_inner">
          <div class="input_area">
            <p class="text_center">なんでもござれ</p>
            <!--methodでpostformであることを宣言
            actionでどこにpostの値を投げるかを指定。action=””だと自分のソースファイルの飛ぶ
            onsubmitでreturnがfalseの場合送信を中止-->
            <!-- actionでinsertへ飛ばす,URLを記載。今回Controller_Bbsのaction_insert() -->
            <form name = "write_form" role="form" action="bbs/insert/" method="post"  onsubmit="return check()">
              <div class="form-group">
                <input type="text" name="user_name" class="form-control" maxlength="8" placeholder="Enter your beautiful name">
                <!-- 入力がなかった際に表示 -->
                <p id="none_input_name" style="display:none; color:red;">なまえを入力してください</p>
              </div>
              <div class="form-group">
                <textarea name="write_area" class="form-control" rows="5" cols="40"></textarea>
                <!-- 入力がなかった際に表示 -->
                <p id="none_input_text" style="display:none; color:red;">本文を入力してください</p>
              </div>
              <div class="input_button">
                <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="とーこー">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- 内容表示するとこ -->
  <div class="list">
    <div class="contents">
      <?php
      $count=1;
      foreach ($cursor as $value){
        ?>
        <div class="article">
          <div class="article_over">
            <span><?php echo $count; ?></span>
            <span class="name">名前 : <?php echo $value['user_name']; ?></span>
            <span class="date"><?php echo date('Y年m月d日', $value['timestamp']);?></span>
            <span class="time" value=<?php $value['timestamp']?>>
              <?php
              if(empty($value['time2'])){
                echo nl2br("\n");//改行
                echo ($value['time']);
              }
              else{
                echo nl2br("\n");//改行
                echo($value['time']."　");
                echo($value['time2']);
              }
              ?>
            </span>
          </div>
          <div class="article_under">
            <p><?php echo nl2br($value['write_area']); ?></p>
          </div>
        </div>
        <?php
        $count++;
      }
      ?>
    </div>
  </div>
</body>
<script src="../assets/javascripts/bootstrap.js"></script>
</html>
