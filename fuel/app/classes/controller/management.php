<?php
class Controller_Management extends Controller{

  /*management.php(ビュー)に投稿を一覧表示するメソッド*/
  public function action_index(){
    $data=[];
    $data['cursor']=Model_Bbs::findMongo();

    foreach ($data['cursor'] as $key => $value) {
      if($value['timestamp'] === $value['edit_timestamp'])
      {
        $data['cursor'][$key]['time'] = '投稿時間:'.date('H時i分s秒',$value['timestamp']);
      }
      else {
        $data['cursor'][$key]['time'] = '投稿時間'.date('H時i分s秒',$value['timestamp']);
        $data['cursor'][$key]['time2']= '編集時間'.date('H時i分s秒',$value['edit_timestamp']);
      }
    }
    return View::forge('welcome/management',$data);
  }

  /*内容を更新するメソッド*/
  public function action_update(){
    $array=Input::post();
    Model_Bbs::updateMongo($array);
    Response::redirect('management/index','refresh', 200);
  }

  /*内容を削除するメソッド*/
  public function action_delete(){
    $array=Input::post();
    Model_Bbs::deleteMongo($array);
    Response::redirect('management/index','refresh', 200);
  }
}
