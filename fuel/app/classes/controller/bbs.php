<?php
class Controller_Bbs extends Controller{

  /*投稿を一覧表示するメソッド*/
  public function action_index(){
    //ビューに渡すデータ配列を初期化
    $data=[];
    // Model_BbsのfindMongo()を呼び出す
    $data['cursor']=Model_Bbs::findMongo();

    // タイムスタンプの処理
    foreach ($data['cursor'] as $key => $value) {
      // 'timestamp'と'edit_timestamp'が一緒の場合:新規投稿
      if($value['timestamp'] === $value['edit_timestamp'])
      {
        $data['cursor'][$key]['time'] = '投稿時間:'.date('H時i分s秒',$value['timestamp']);
      }
      // 'timestamp'と'edit_timestamp'が違う場合:編集
      else {
        $data['cursor'][$key]['time'] = '投稿時間'.date('H時i分s秒',$value['timestamp']);
        $data['cursor'][$key]['time2']= '編集時間'.date('H時i分s秒',$value['edit_timestamp']);
      }
    }
    // 配列$dataを返してビューへ
    return View::forge('welcome/test',$data);
  }

    /*内容をinsertするメソッド*/
    public function action_insert(){
      //Viewからpostされて来ているか
      $array=Input::post();
      //Model_Bbsのオブジェクトを作成、insertMongoに$arrayを送る
      Model_Bbs::insertMongo($array);
      //insertMongoから返ってきた値をaction_indexに送る
      Response::redirect('index', 'refresh');
    }
}
