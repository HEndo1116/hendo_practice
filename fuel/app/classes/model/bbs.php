<?php

class Model_Bbs extends Model_Crud{

  /*コレクションを検索するメソッド*/
  public static function findMongo(){
    //インスタンスを取得
    $mongo=Mongo_Db::instance();
    $mongo->order_by([
      // 降順にソート
      'timestamp'=>'desc'
    ]);
    //コレクションの検索結果を返す
    $data=$mongo->get('bbs');
    //タイムゾーンを設定
    date_default_timezone_set('Asia/Tokyo');
    return $data;
  }

  /*コレクションにドキュメントをinsertするメソッド*/
  // Controller_Bbsのaction_insertから送られてきたものを受け取る
  public static function insertMongo($array){
    $user_name=$array['user_name'];
    $write_area=$array['write_area'];
    // インスタンスを取得
    $mongo=Mongo_Db::instance();
    // それぞれインサート
    $mongo->insert('bbs',[
      'user_name'=>$user_name,
      'write_area'=>$write_area,
      'timestamp'=>time(),
      'edit_timestamp'=>time()
    ]);
  }

  /*ドキュメントを更新(編集)するメソッド*/
  // Controller_Managementのaction_updateから送られてきたものを受け取る
  public static function updateMongo($array){
    $mongo_id_e=$array['mongo_id_e'];
    $user_name=$array['user_name'];
    $write_area=$array['write_area'];
    // インスタンスを取得
    $mongo=Mongo_Db::instance();
    // アップデートする
    $mongo->where(['_id'=>new MongoId($mongo_id_e)])->update('bbs',[
      'user_name'=>$user_name,
      'write_area'=>$write_area,
      'edit_timestamp'=>time()
    ]);
  }

  /*ドキュメントを削除するメソッド*/
  // Controller_Managementのaction_deleteから送られてきたものを受け取る
  public static function deleteMongo($array){
    $mongo_id_d=$array['mongo_id_d'];
    // インスタンスを取得
    $mongo=Mongo_Db::instance();
    //どきゅめんとを削除
    $mongo->where(['_id'=>new MongoId($mongo_id_d)])->delete('bbs');
  }
}
